#include <board.hpp>
#include <board/iterators.hpp>
#include <gtest/gtest.h>
#include <piece.hpp>

using namespace engine;

static auto piece_count(piece_iterator begin, piece_iterator end) -> int {
  int pieces_found = 0;
  while (begin != end) {
    ++begin;
    pieces_found++;
  }

  return pieces_found;
}

TEST(board, piece_iterators_none) {
  std::array<engine::square, 8 * 8> pieces_map;
  auto board = engine::board{std::move(pieces_map)};

  auto beg = board.pieces_begin();
  auto end = board.pieces_end();

  ASSERT_EQ(piece_count(beg, end), 0);
}

TEST(board, piece_iterators_one) {
  piece p = piece{
      .position = position{0, 1}, .colour = colour::Black, .type = queen{}};
  std::array<engine::square, 8 * 8> pieces_map{};

  pieces_map[1] = engine::square::with(std::move(p));
  auto board = engine::board{std::move(pieces_map)};

  auto beg = board.pieces_begin();
  auto end = board.pieces_end();

  ASSERT_EQ(piece_count(beg, end), 1);
}

TEST(board, piece_iterators_many) {
  std::array<engine::square, 8 * 8> pieces_map{};

  for (int i = 0; i < 8 * 8; ++i) {
    auto p = position{i % 8, i / 8};
    auto piece = engine::piece{.position = std::move(p)};
    auto index = std::size_t{piece.position};
    pieces_map[index] = engine::square::with(std::move(piece));
  }

  auto board = engine::board{std::move(pieces_map)};

  auto beg = board.pieces_begin();
  auto end = board.pieces_end();

  ASSERT_EQ(piece_count(beg, end), 64);
}
