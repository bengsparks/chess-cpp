#pragma once

#include <board/position.hpp>

namespace error {

struct position_e1 {
  engine::position lhs, rhs;
  char const* const op;
};

struct position_e2 {
  engine::position p;
  int coeff;
};

};  // namespace error