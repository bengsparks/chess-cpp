#pragma once

#include <errors.hpp>

#include <unordered_map>
#include "board.hpp"
#include "move.hpp"
#include "piece.hpp"

namespace engine {

struct context {
 private:
  explicit context(board& board);

 public:
  board& board;
  std::unordered_multimap<piece&, move, piece_hasher> moves_;

  static auto lazy(class board& board) -> context;

  static auto greedy(class board& board) -> engine_result<context>;
};

};  // namespace engine