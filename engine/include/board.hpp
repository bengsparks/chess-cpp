#pragma once

#include <array>
#include <board/iterators.hpp>
#include <board/square.hpp>

namespace engine {

class board {
  std::array<square, 8 * 8> squares_;

 public:
  explicit board(std::array<square, 8 * 8> squares);

  [[nodiscard]] auto at(position const& p) -> square&;

  [[nodiscard]] auto at(position const& p) const -> square const&;

  [[nodiscard]] auto squares_begin() const -> const_square_iterator;

  [[nodiscard]] auto squares_begin() -> square_iterator;

  [[nodiscard]] auto squares_end() const -> const_square_iterator;

  [[nodiscard]] auto squares_end() -> square_iterator;

  [[nodiscard]] auto pieces_begin() -> piece_iterator;

  [[nodiscard]] auto pieces_begin() const -> const_piece_iterator;

  [[nodiscard]] auto pieces_end() -> piece_iterator;

  [[nodiscard]] auto pieces_end() const -> const_piece_iterator;
};

}  // namespace engine