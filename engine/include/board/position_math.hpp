#pragma once

#include <errors.hpp>

namespace engine {

using math_r = engine_result<position>;

class position;

auto operator+(position const& lhs, position const& rhs) -> math_r;

auto operator-(position const& lhs, position const& rhs) -> math_r;

auto operator*(position const& lhs, int rhs) -> engine_result<position>;

};  // namespace engine