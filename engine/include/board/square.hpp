#pragma once

#include <optional>

#include "piece.hpp"

namespace engine {

class square {
  std::optional<piece> piece_;

 public:
  square();

  explicit square(piece p);

  [[nodiscard]] static auto empty() -> square;

  [[nodiscard]] static auto with(piece p) -> square;

  [[nodiscard]] auto on() -> std::optional<piece>&;

  [[nodiscard]] auto on() const -> std::optional<piece> const&;

  [[nodiscard]] auto has_piece() const -> bool;
};

}  // namespace engine