#pragma once

#include <cstdlib>

namespace engine {

class position {
 public:
  int x, y;

  position(int x, int y);

  explicit operator std::size_t() const;

  position(position const&) = delete;

  auto operator=(position const&) -> position& = delete;

  position(position&&) = default;

  auto operator=(position&&) -> position& = default;

  [[nodiscard]] auto clone() const -> position;

  auto operator!=(position const& other) const -> bool;
  auto operator==(position const& other) const -> bool;
};

};  // namespace engine