#pragma once

#include <board/position.hpp>
#include <concepts>

#include "square.hpp"

namespace engine {

class board;

class piece_iterator;

class const_piece_iterator;

class square_iterator {
  board& board_;
  position position_;

  friend piece_iterator;

 public:
  explicit square_iterator(board& board,
                           position position = engine::position{0, 0});

  square_iterator(square_iterator const& other);

  auto operator!=(square_iterator const& other) const -> bool;

  auto operator++(int) -> square_iterator;

  auto operator++() -> square_iterator&;

  auto operator*() const -> square&;

  auto operator->() const -> square*;
};

class const_square_iterator {
  board const& board_;
  position position_;

  friend const_piece_iterator;

 public:
  explicit const_square_iterator(board const& board,
                                 position position = engine::position{0, 0});

  const_square_iterator(const_square_iterator const& other);

  auto operator!=(const_square_iterator const& other) const -> bool;

  auto operator++(int) -> const_square_iterator;

  auto operator++() -> const_square_iterator&;

  auto operator*() const -> square const&;

  auto operator->() const -> square const* const;
};

class piece_iterator {
  square_iterator siter_;

 public:
  explicit piece_iterator(board& board,
                          position position = engine::position{0, 0});

  piece_iterator(piece_iterator const& other) = default;

  auto operator!=(piece_iterator const& other) const -> bool;

  auto operator++(int) -> piece_iterator;

  auto operator++() -> piece_iterator&;

  auto operator*() const -> piece&;

  auto operator->() const -> piece*;
};

class const_piece_iterator {
  const_square_iterator siter_;

 public:
  explicit const_piece_iterator(board const& board,
                                position position = engine::position{0, 0});

  const_piece_iterator(const_piece_iterator const& other) = default;

  auto operator!=(const_piece_iterator const& other) const -> bool;

  auto operator++(int) -> const_piece_iterator;

  auto operator++() -> const_piece_iterator&;

  auto operator*() const -> piece const&;

  auto operator->() const -> piece const*;
};

};  // namespace engine
