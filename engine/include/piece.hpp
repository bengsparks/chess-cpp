#pragma once

#include <variant>
#include <vector>

#include <board/position.hpp>
#include "piece_type.hpp"

namespace engine {

enum class colour { White, Black };

struct piece {
  position position;
  colour colour;
  piece_type type;
};

auto operator==(const piece& lhs, const piece& rhs) -> bool;

}  // namespace engine

template <>
struct std::hash<engine::piece> {
  auto operator()(const engine::piece& p) const noexcept -> std::size_t;
};

namespace engine {
using piece_hasher = ::std::hash<piece>;
}