#pragma once

#include "../board/square.hpp"
#include "../piece.hpp"

namespace engine {

class move_hasher;

class simple {
  friend move_hasher;

  piece const& piece_;
  square const& to_;

 public:
  simple(piece const& piece, square const& to);
};

};  // namespace engine