#pragma once

#include <board/square.hpp>

namespace engine {

class piece;

class square;

class move_hasher;

class castle {
  friend move_hasher;

  piece& rook_;
  square& rook_to_;
  piece& king_;
  square& king_to_;

 public:
  castle(piece& rook, square& rook_to, piece& king, square& king_to);
};

};  // namespace engine