#pragma once

#include <variant>

namespace {
    template <typename T, typename R, typename... Params>
    struct visitor_method {
        virtual auto operator()(T const&, Params const&... params) const -> R = 0;
    };
};

template <typename R, typename T, typename... Params>
struct visitor;


template <typename R, typename... Ts, typename... Params>
struct visitor<R, std::variant<Ts...>, Params...> : visitor_method<Ts, R, Params...>... {};