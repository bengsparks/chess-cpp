#pragma once

#include <variant>

#include "moves/castle.hpp"
#include "moves/simple.hpp"

#include <utils/visiting.hpp>

namespace engine {

using move = std::variant<simple, castle>;

template <typename R>
using move_visitor = visitor<R, move>;

struct move_hasher final : move_visitor<std::size_t> {
  auto operator()(simple const&) const -> std::size_t override;

  auto operator()(castle const&) const -> std::size_t override;
};

}  // namespace engine

template <>
struct std::hash<engine::move> {
  engine::move_hasher hasher{};

 public:
  auto operator()(engine::move const& m) const noexcept -> std::size_t;
};