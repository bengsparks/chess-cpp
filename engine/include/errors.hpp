#pragma once

#include <utility>
#include <variant>

#include "errors/position.hpp"

using engine_error = std::variant<error::position_e1, error::position_e2>;

template <typename T>
class engine_result {
  std::variant<T, engine_error> v_;

 public:
  engine_result(T t) : v_{std::move(t)} {}

  engine_result(engine_error ee) : v_{std::move(ee)} {}

  template <typename U>
  auto propagate_err() -> engine_result<U> {
    return engine_result<U>::Err(std::move(this->err()));
  }

  auto value() -> T& { return std::get<0>(this->v_); }

  auto value() const -> T const& { return std::get<0>(this->v_); }

  auto err() -> engine_error& { return std::get<1>(this->v_); }

  auto err() const -> engine_error const& { return std::get<1>(this->v_); }

  [[nodiscard]] auto is_ok() const -> bool { return this->v_.index() == 0; }

  [[nodiscard]] auto is_err() const -> bool { return !(this->is_ok()); }

  static auto Ok(T t) -> engine_result<T> { return std::move(t); }

  static auto Err(engine_error ee) -> engine_result<T> { return std::move(ee); }
};
