#pragma once

#include <string_view>
#include <vector>

#include <context.hpp>
#include <errors.hpp>
#include <move.hpp>
#include "../piece_type.hpp"
#include "../utils/visiting.hpp"

namespace engine {

struct piece_move_finder_visitor final
    : piece_visitor<engine_result<std::vector<move>>> {
 private:
  board const& board_;
  const piece *piece_;

 public:
  piece_move_finder_visitor() = delete;

  explicit piece_move_finder_visitor(board const& board, piece const* piece = nullptr)
      : board_{board}, piece_{piece} {}

  auto set_piece(piece const* piece) -> void {
      this->piece_ = piece;
  }

  auto operator()(queen const& q) const -> engine_result<std::vector<move>> override;

  auto operator()(rook const& r) const -> engine_result<std::vector<move>> override;
};

struct piece_name_visitor final : piece_visitor<std::string_view> {
  auto operator()(queen const& q) const -> std::string_view override;

  auto operator()(rook const& r) const -> std::string_view override;
};

};  // namespace engine