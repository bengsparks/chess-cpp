#pragma once

#include "pieces/queen.hpp"
#include "pieces/rook.hpp"
#include "utils/visiting.hpp"

namespace engine {

using piece_type = std::variant<queen, rook>;

template <typename R, typename... Params>
using piece_visitor = visitor<R, piece_type, Params...>;

};  // namespace engine