#include <move.hpp>

auto std::hash<engine::move>::operator()(engine::move const& m) const noexcept -> std::size_t {
  return std::visit(this->hasher, m);
}