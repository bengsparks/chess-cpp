#include <board.hpp>
#include <board/iterators.hpp>

using namespace engine;

static auto const out_of_bounds = position{0, 8};

board::board(std::array<square, 8 * 8> squares)
    : squares_{std::move(squares)} {}

auto board::at(const position& p) -> square& {
  return this->squares_[std::size_t{p}];
}

auto board::at(const position& p) const -> square const& {
  return this->squares_[std::size_t{p}];
}

auto board::squares_begin() -> square_iterator {
  return engine::square_iterator(*this);
}

auto board::squares_begin() const -> const_square_iterator {
  return engine::const_square_iterator(*this);
}

auto board::squares_end() -> square_iterator {
  return engine::square_iterator{*this, out_of_bounds.clone()};
}

auto board::squares_end() const -> const_square_iterator {
  return engine::const_square_iterator{*this, out_of_bounds.clone()};
}

auto board::pieces_begin() -> piece_iterator {
  return engine::piece_iterator{*this};
}

auto board::pieces_begin() const -> const_piece_iterator {
  return engine::const_piece_iterator{*this};
}

auto board::pieces_end() -> piece_iterator {
  return engine::piece_iterator{*this, out_of_bounds.clone()};
}

auto board::pieces_end() const -> const_piece_iterator {
  return const_piece_iterator{*this, out_of_bounds.clone()};
}
