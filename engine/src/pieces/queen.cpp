#include <board/position_math.hpp>
#include <pieces/visitors.hpp>

using namespace engine;

auto piece_move_finder_visitor::operator()(queen const& q) const
    -> engine_result<std::vector<move>> {
  static const auto normed_offsets = std::array<position, 8>{
      {position{1, 1}, position{1, -1}, position{-1, 1}, position{-1, -1},
       position{0, 1}, position{0, -1}, position{0, 1}, position{-1, 0}}};

  auto offsets = std::vector<move>{};
  offsets.reserve(3 * 7);

  for (auto const& norm_offset : normed_offsets) {
    for (int i = 0; i < 8; ++i) {
      // relative offset (cannot fail)
      auto const& offset = (norm_offset * i).value();

      // absolute position. next offset if this goes OOB
      auto const pos = offset + this->piece_->position;
      if (pos.is_err()) {
        break;
      }

      auto const& square = this->board_.at(pos.value());
      // Capture piece on this position; cannot go any further; next offset
      if (square.has_piece() && square.on()->colour != this->piece_->colour) {
        offsets.emplace_back(simple{*this->piece_, square});
        break;
      }

      // Nothing here; viable square; same offset next iteration
      offsets.emplace_back(simple{*this->piece_, square});
    }
  }

  return offsets;
}
