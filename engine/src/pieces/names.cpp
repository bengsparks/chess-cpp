#include <string>

#include <pieces/visitors.hpp>

using namespace engine;

auto piece_name_visitor::operator()(queen const& queen) const
    -> std::string_view {
  static auto const queen_n = std::string{"queen"};
  return queen_n;
}

auto piece_name_visitor::operator()(rook const& rook) const
    -> std::string_view {
  static auto const rook_n = std::string{"rook"};
  return rook_n;
}
