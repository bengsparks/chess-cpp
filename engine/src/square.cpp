#include <board/square.hpp>

using namespace engine;

square::square() : piece_{std::nullopt} {}

square::square(piece p) : piece_{std::move(p)} {}

auto square::empty() -> square {
  return square{};
}

auto square::with(piece p) -> square {
  return square{std::move(p)};
}

auto square::on() -> std::optional<piece>& {
  return this->piece_;
}

auto square::on() const -> const std::optional<piece>& {
  return this->piece_;
}

auto square::has_piece() const -> bool {
  return this->piece_.has_value();
}
