#include <context.hpp>
#include <functional>
#include <pieces/visitors.hpp>

#include <move.hpp>

using namespace engine;

context::context(class board& board) : board{board}, moves_{} {}

auto context::lazy(class board& board) -> context {
  return context{board};
}

auto context::greedy(class board& board) -> engine_result<context> {
  auto ctxt = context::lazy(board);

  auto begin = board.pieces_begin();
  auto end = board.pieces_end();

  auto move_finder = piece_move_finder_visitor{board};

  while (begin != end) {
    auto& p = *begin++;
    move_finder.set_piece(&p);
    auto moves = std::visit(move_finder, p.type);

    if (moves.is_err()) {
      return moves.propagate_err<context>();
    }

    for (auto& m : moves.value()) {
        auto node = std::make_pair<std::reference_wrapper<piece>, move>(p, std::move(m));
        ctxt.moves_.insert(std::move(node));
    }
  }

  return engine_result<context>::Ok(std::move(ctxt));
}
