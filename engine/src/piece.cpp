#include <piece.hpp>

auto std::hash<engine::piece>::operator()(const engine::piece& p) const noexcept
    -> std::size_t {
  auto start = std::size_t{31};

  // Only one piece per square; hashing of position suffices
  auto index = std::size_t{p.position};
  start |= std::hash<std::size_t>{}(index);

  return start;
}

auto engine::operator==(const engine::piece& lhs, const engine::piece& rhs)
    -> bool {
  return lhs.position == rhs.position;
}
