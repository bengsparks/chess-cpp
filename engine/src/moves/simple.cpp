#include <move.hpp>
#include <moves/simple.hpp>

using namespace engine;

simple::simple(piece const& piece, square const& to) : piece_{piece}, to_{to} {}

auto move_hasher::operator()(engine::simple const& simple) const -> std::size_t {
  std::size_t start = 31;

  start |= std::hash<piece const*>{}(std::addressof(simple.piece_));
  start |= std::hash<square const*>{}(std::addressof(simple.to_));

  return start;
}
