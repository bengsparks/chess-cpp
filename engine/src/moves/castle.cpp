#include <move.hpp>
#include <moves/castle.hpp>

using namespace engine;

castle::castle(piece& rook, square& rook_to, piece& king, square& king_to)
    : rook_{rook}, rook_to_{rook_to}, king_{king}, king_to_{king_to} {}

auto move_hasher::operator()(engine::castle const& castle) const -> std::size_t {
  std::size_t start = 31;

  start |= std::hash<piece const*>{}(std::addressof(castle.king_));
  start |= std::hash<piece const*>{}(std::addressof(castle.rook_));

  start |= std::hash<square const*>{}(std::addressof(castle.king_to_));
  start |= std::hash<square const*>{}(std::addressof(castle.rook_to_));

  return start;
}
