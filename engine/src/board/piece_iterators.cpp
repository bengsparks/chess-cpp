#include <board.hpp>

#include <board/iterators.hpp>

auto const static out_of_bounds = engine::position{0, 8};

namespace engine {

piece_iterator::piece_iterator(board& board, position position)
    : siter_{board, std::move(position)} {
  if (this->siter_->has_piece() || this->siter_.position_ == out_of_bounds) {
    return;
  }

  ++*this;
}

auto piece_iterator::operator!=(const piece_iterator& other) const -> bool {
  return this->siter_ != other.siter_;
}

auto piece_iterator::operator++() -> piece_iterator& {
  do {
    ++this->siter_;
  } while (!this->siter_->has_piece() &&
           this->siter_.position_ != out_of_bounds);

  return *this;
}

auto piece_iterator::operator++(int) -> piece_iterator {
  auto copy = *this;
  ++*this;
  return copy;
}

auto piece_iterator::operator*() const -> piece& {
  auto& square = *this->siter_;
  return square.on().value();
}

auto piece_iterator::operator->() const -> piece* {
  auto& square = *this->siter_;
  return &square.on().value();
}

const_piece_iterator::const_piece_iterator(const board& board,
                                           position position)
    : siter_{board, std::move(position)} {
  if (this->siter_->has_piece() || this->siter_.position_ == out_of_bounds) {
    return;
  }

  ++*this;
}

auto const_piece_iterator::operator!=(const_piece_iterator const& other) const
    -> bool {
  return this->siter_ != other.siter_;
}

auto const_piece_iterator::operator++() -> const_piece_iterator& {
  do {
    ++this->siter_;
  } while (!this->siter_->has_piece() &&
           this->siter_.position_ != out_of_bounds);

  return *this;
}

auto const_piece_iterator::operator++(int) -> const_piece_iterator {
  auto copy = *this;
  ++*this;
  return copy;
}

auto const_piece_iterator::operator*() const -> piece const& {
  const auto& square = *this->siter_;
  return square.on().value();
}

auto const_piece_iterator::operator->() const -> piece const* {
  const auto& square = *this->siter_;
  return &square.on().value();
}

};  // namespace engine