#include <board/position.hpp>

#include <errors.hpp>

using namespace engine;

position::position(int x, int y) : x{x}, y{y} {}

position::operator std::size_t() const {
  return this->y * 8 + this->x;
}

auto position::clone() const -> position {
  return position{this->x, this->y};
}

auto position::operator==(const position& other) const -> bool {
  return !(*this != other);
}

auto position::operator!=(position const& other) const -> bool {
  return this->x != other.x || this->y != other.y;
}
