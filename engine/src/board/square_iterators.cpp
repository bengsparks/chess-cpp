#include <board.hpp>
#include <board/iterators.hpp>

using namespace engine;

square_iterator::square_iterator(board& board, position position)
    : board_{board}, position_{std::move(position)} {}

square_iterator::square_iterator(const square_iterator& other)
    : board_{other.board_}, position_{other.position_.clone()} {}

auto square_iterator::operator++(int) -> square_iterator {
  auto copy = *this;
  ++*this;
  return copy;
}

auto square_iterator::operator++() -> square_iterator& {
  if (this->position_.x < 7) {
    this->position_.x++;
  } else {
    this->position_.x = 0;
    this->position_.y++;
  }

  return *this;
}

auto square_iterator::operator*() const -> square& {
  return this->board_.at(this->position_);
}

auto square_iterator::operator!=(const square_iterator& other) const -> bool {
  return std::addressof(this->board_) != std::addressof(other.board_) ||
         this->position_ != other.position_;
}

auto square_iterator::operator->() const -> square* {
  return &this->board_.at(this->position_);
}

const_square_iterator::const_square_iterator(const board& board,
                                             position position)
    : board_{board}, position_{position.clone()} {}

const_square_iterator::const_square_iterator(const const_square_iterator& other)
    : board_{other.board_}, position_{other.position_.clone()} {}

auto const_square_iterator::operator++() -> const_square_iterator& {
  if (this->position_.x < 7) {
    this->position_.x++;
  } else {
    this->position_.x = 0;
    this->position_.y++;
  }

  return *this;
}

auto const_square_iterator::operator++(int) -> const_square_iterator {
  auto copy = *this;
  ++*this;
  return copy;
}

auto const_square_iterator::operator*() const -> square const& {
  return this->board_.at(this->position_);
}

auto const_square_iterator::operator!=(const const_square_iterator& other) const
    -> bool {
  return std::addressof(this->board_) != std::addressof(other.board_) ||
         this->position_ != other.position_;
}

auto const_square_iterator::operator->() const -> square const* const {
  return &this->board_.at(this->position_);
}
