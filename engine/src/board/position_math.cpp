#include <board/position_math.hpp>

using namespace engine;

static auto in_bounds(int index) -> bool {
  return 0 <= index && index < 8;
}

auto operator+(const position& lhs, const position& rhs) -> math_r {
  auto const x = lhs.x + rhs.x;
  auto const y = lhs.y + rhs.y;

  if (!in_bounds(x) || !in_bounds(y)) {
    engine_error e = error::position_e1{.lhs = std::move(lhs.clone()),
                                        .rhs = std::move(rhs.clone()),
                                        .op = "+"};
    return std::move(e);
  }

  return position{x, y};
}

auto operator-(position const& lhs, position const& rhs) -> math_r {
  auto const x = lhs.x - rhs.x;
  auto const y = lhs.y - rhs.y;

  if (!in_bounds(x) || !in_bounds(y)) {
    engine_error e = error::position_e1{.lhs = std::move(lhs.clone()),
                                        .rhs = std::move(rhs.clone()),
                                        .op = "-"};
    return std::move(e);
  }

  return position{x, y};
}

auto operator*(position const& lhs, int coeff) -> engine_result<position> {
  auto const x = lhs.x * coeff;
  auto const y = lhs.y * coeff;

  if (!in_bounds(x) || !in_bounds(y)) {
    engine_error e =
        error::position_e2{.p = std::move(lhs.clone()), .coeff = coeff};
    return std::move(e);
  }

  return position{x, y};
}
